<?php include("header.php")?>
      <div class="container"><div class="caption page-top clearfix">
        <div class="pull-left"><h2>Gallery</h2></div>
        <ol class="breadcrumb pull-right">
          <li><a href="index.php">Home</a></li>
          <li class="active">Gallery</li>
        </ol>
      </div></div>
    </div>
  </div>
  <!--Inner page content-->
  <section class="inner-content padding-20">
    <div class="container">
      <!-- Masonry wrapper -->
                    	<div class="wp-masonry-wrapper wp-masonry-3-cols">
                        <div class="wp-masonry-gutter"></div>
                        <div class="wp-masonry-block">
                          <div class="post-item style1">
                            <div class="post-content-wr">
                                <div class="post-meta-top">
                                    <div class="post-image">
                                      <a href="img/timthumb.jpg" class="popup-img">
                                                                                  <img src="img/timthumb.jpg" alt="">
                                                                              </a>
                                    </div>
                                    
                                </div>
                                
                                
                            </div>
                          </div>
                        </div>
                        <div class="wp-masonry-block">
                          <div class="post-item style1">
                            <div class="post-content-wr">
                                <div class="post-meta-top">
                                    <div class="post-image">
                                      <a href="img/timthumb (1).jpg" class="popup-img">
                                                                                  <img src="img/timthumb (1).jpg" alt="">
                                                                              </a>
                                    </div>
                                    
                                </div>
                                
                                
                            </div>
                          </div>
                        </div>
                       <div class="wp-masonry-block">
                          <div class="post-item style1">
                            <div class="post-content-wr">
                                <div class="post-meta-top">
                                    <div class="post-image">
                                      <a href="img/timthumb (1).jpg" class="popup-img">
                                                                                  <img src="img/timthumb (1).jpg" alt="">
                                                                              </a>
                                    </div>
                                    
                                </div>
                                
                                
                            </div>
                          </div>
                        </div>
                         
                        <div class="wp-masonry-block">
                          <div class="post-item style1">
                            <div class="post-content-wr">
                                <div class="post-meta-top">
                                    <div class="post-image">
                                      <a href="img/timthumb.jpg" class="popup-img">
                                                                                  <img src="img/timthumb.jpg" alt="">
                                                                              </a>
                                    </div>
                                    
                                </div>
                                
                                
                            </div>
                          </div>
                        </div>
                       <div class="wp-masonry-block">
                          <div class="post-item style1">
                            <div class="post-content-wr">
                                <div class="post-meta-top">
                                    <div class="post-image">
                                      <a href="img/timthumb (1).jpg" class="popup-img">
                                                                                  <img src="img/timthumb (1).jpg" alt="">
                                                                              </a>
                                    </div>
                                    
                                </div>
                                
                                
                            </div>
                          </div>
                        </div> 
                        <div class="wp-masonry-block">
                          <div class="post-item style1">
                            <div class="post-content-wr">
                                <div class="post-meta-top">
                                    <div class="post-image">
                                      <a href="img/timthumb.jpg" class="popup-img">
                                                                                  <img src="img/timthumb.jpg" alt="">
                                                                              </a>
                                    </div>
                                    
                                </div>
                                
                                
                            </div>
                          </div>
                        </div>
                       <div class="wp-masonry-block">
                          <div class="post-item style1">
                            <div class="post-content-wr">
                                <div class="post-meta-top">
                                    <div class="post-image">
                                      <a href="img/timthumb (1).jpg" class="popup-img">
                                                                                  <img src="img/timthumb (1).jpg" alt="">
                                                                              </a>
                                    </div>
                                    
                                </div>
                                
                                
                            </div>
                          </div>
                        </div> 
                        <div class="wp-masonry-block">
                          <div class="post-item style1">
                            <div class="post-content-wr">
                                <div class="post-meta-top">
                                    <div class="post-image">
                                      <a href="img/timthumb.jpg" class="popup-img">
                                                                                  <img src="img/timthumb.jpg" alt="">
                                                                              </a>
                                    </div>
                                    
                                </div>
                                
                                
                            </div>
                          </div>
                        </div>
                       <div class="wp-masonry-block">
                          <div class="post-item style1">
                            <div class="post-content-wr">
                                <div class="post-meta-top">
                                    <div class="post-image">
                                      <a href="img/timthumb (1).jpg" class="popup-img">
                                                                                  <img src="img/timthumb (1).jpg" alt="">
                                                                              </a>
                                    </div>
                                    
                                </div>
                                
                                
                            </div>
                          </div>
                        </div>
                         
                        <div class="wp-masonry-block">
                          <div class="post-item style1">
                            <div class="post-content-wr">
                                <div class="post-meta-top">
                                    <div class="post-image">
                                      <a href="img/timthumb.jpg" class="popup-img">
                                                                                  <img src="img/timthumb.jpg" alt="">
                                                                              </a>
                                    </div>
                                    
                                </div>
                                
                                
                            </div>
                          </div>
                        </div>
                       <div class="wp-masonry-block">
                          <div class="post-item style1">
                            <div class="post-content-wr">
                                <div class="post-meta-top">
                                    <div class="post-image">
                                      <a href="img/timthumb (1).jpg" class="popup-img">
                                                                                  <img src="img/timthumb (1).jpg" alt="">
                                                                              </a>
                                    </div>
                                    
                                </div>
                                
                                
                            </div>
                          </div>
                        </div>
                      </div>
     
      
    </div>
  </section>
  <!--Inner page content-->
  <?php include("footer.php")?>
