<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>A4Z Printing Press</title>

<!-- Bootstrap -->
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!--Google Fonts-->
<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Passion+One:400,700,900" rel="stylesheet">
<!--Icon Fonts-->
<link href="fonts/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="fonts/simple-line-icons-master/css/simple-line-icons.css" rel="stylesheet">
<!--Icon Fonts-->
<!-- Plugins -->
<link href="plugins/magnific-popup/magnific-popup.css" rel="stylesheet">
<!--Core Css-->

<link href="css/style.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<!-- scrollToTop -->
    <!-- ================ -->
    <div class="scrollToTop"><i class="fa fa-angle-up"></i></div>
<div class="page-wrapper"> 
   <header class="clearfix">
      <div class="container">
        <div class="row">
        <div class="col-md-3"> <div class=" logo"><a href="index.php"><img src="img/A4Z-LOGO-FINAL-copy.png" alt="A4Z Printing Press"> <span>A4Z Printing Press</span> </a> </div>
        </div>
        <div class="col-md-9">
          <div class="header-right">
          <div class="top-header hidden-xs">
            <ul class="header-contact">
              <li> <i class="icon-location-pin icons"></i> <b>Address</b> <span> Newroad, Pokhara, Nepal</span> </li>
              <li> <i class="icon-phone icons"></i> <b>Call Us</b> <span>+977-61-522751</span> </li>
              <li> <i class="icon-envelope icons"></i> <b>Mail Us</b> <span>a4zgdpp@gmail.com</span> </li>
            </ul>
          </div>
          <nav class="navbar navbar-default">
           
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                <a class="navbar-brand" href="#">Menu</a> </div>
              
              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                  <li class="active"><a href="index.php">Home <span class="sr-only">(current)</span></a></li>
                  <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">About Us <span class="fa fa-angle-down"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="comapny-profile.php">Company Profile</a></li>
                      <li><a href="team.php">Management Team</a></li>
                    </ul>
                  </li>
                  <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Services<span class="fa fa-angle-down"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">Offset</a></li>
                      <li><a href="#">Screen Print</a></li>
                      <li><a href="#">Flex Print</a></li>
                      <li><a href="#">Rubber Print</a></li>
                    </ul>
                  </li>
                  <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Training<span class="fa fa-angle-down"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">Basic Graphic Design</a></li>
                      <li><a href="#">Advanced Graphic Training</a></li>
                      <li><a href="#">Diploma in computer Training</a></li>
                      <li><a href="#">Basic in Computer Training</a></li>
                      <li><a href="#">Webpage Designing (Basic /Design)</a></li>
                      <li><a href="#">Hardware and Networking (Basic/Advance)</a></li>
                      <li><a href="#">Accounting Training</a></li>
                    </ul>
                  </li>
                  <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Our Associate<span class="fa fa-angle-down"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">A4z graphic Academy</a></li>
                      <li><a href="#">A4z computer solution and service</a></li>
                      <li><a href="#">Reason Electronic and electrical Mobile</a></li>
                      <li><a href="#">Shrestha Fresh House</a></li>
                      <li><a href="#">Shree Krishna Hotel</a></li>
                    </ul>
                  </li>
                  <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Media Partner<span class="fa fa-angle-down"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">Sarangkot Weekly</a></li>
                      <li><a href="#">Public Aawaj</a></li>
                      <li><a href="#">Nada Express</a></li>
                      <li><a href="#">Syangja Sandesh</a></li>
                      <li><a href="#">Machhapuchhre Weekly</a></li>
                    </ul>
                  </li>
                  <li><a href="gallery.php">Gallery</a></li>
                  <li><a href="contact.php">Contact Us</a></li>
                </ul>
               
              </div>
              <!-- /.navbar-collapse --> 
           
          </nav>
        </div>
        </div>
      </div>
        </div>
        <!--Header end--> 
        
      </header>