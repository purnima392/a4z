<?php include("header.php")?>
   <!--Banner Start-->
  
    <div class="container"> 
    <div class="banner">
      <!--Header Start-->
     
    <div class="caption text-right">
        <h2>Design & Print</h2>
        <h3>We Provide your business with all of your printing needs.</h3>
      </div>
    </div>
  </div>
  <!--Banner End--> 
<!--Service Section Start-->
  <div class="service-section">
    <div class="container">
      <div class="row">
        <div class="col-md-3 col-sm-4 col-xs-6">
          <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
            <div class="flipper box-wrapper effect2 text-center">
              <div class="front"> 
                <!-- front content -->
                <article> <a href="#"><i class="icon-calendar icons"></i></a>
                  <h4>Print <span>Calendars</span></h4>
                  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</p>
                  <a href="services.php" class="btn btn-warning">Read More <i class="fa fa-chevron-circle-right"></i></a> </article>
              </div>
              <div class="back"> 
                <!-- back content -->
                <article> <a href="#"><i class="icon-calendar icons"></i></a>
                  <h4>Print <span>Calendars</span></h4>
                  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</p>
                  <a href="services.php" class="btn btn-warning">Read More <i class="fa fa-chevron-circle-right"></i></a> </article>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6">
          <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
            <div class="flipper box-wrapper effect2 text-center ">
              <div class="front"> 
                <!-- front content -->
                <article> <a href="#"><i class="fa fa-paint-brush"></i></a>
                  <h4>Digital <span>Color Prints</span></h4>
                  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</p>
                  <a href="services.php" class="btn btn-warning">Read More <i class="fa fa-chevron-circle-right"></i></a> </article>
              </div>
              <div class="back"> 
                <!-- back content -->
                <article> <a href="#"><i class="fa fa-paint-brush"></i></a>
                  <h4>Digital <span>Color Prints</span></h4>
                  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</p>
                  <a href="services.php" class="btn btn-warning">Read More <i class="fa fa-chevron-circle-right"></i></a> </article>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6">
          <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
            <div class="flipper box-wrapper effect2 text-center">
              <div class="front"> 
                <!-- front content -->
                <article> <a href="#"><i class="icon-book-open icons"></i></a>
                  <h4>Print <span>Booklet</span></h4>
                  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</p>
                  <a href="services.php" class="btn btn-warning">Read More <i class="fa fa-chevron-circle-right"></i></a> </article>
              </div>
              <div class="back"> 
                <!-- back content -->
                <article> <a href="#"><i class="icon-book-open icons"></i></a>
                  <h4>Print <span>Booklet</span></h4>
                  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</p>
                  <a href="services.php" class="btn btn-warning">Read More <i class="fa fa-chevron-circle-right"></i></a> </article>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6">
          <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
            <div class="flipper box-wrapper effect2 text-center">
              <div class="front"> 
                <!-- front content -->
                <article> <a href="#"><i class="icon-layers icons"></i></a>
                  <h4>Design <span>Menus</span></h4>
                  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</p>
                  <a href="services.php" class="btn btn-warning">Read More <i class="fa fa-chevron-circle-right"></i></a> </article>
              </div>
              <div class="back"> 
                <!-- back content -->
                <article> <a href="#"><i class="icon-layers icons"></i></a>
                  <h4>Design <span>Menus</span></h4>
                  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</p>
                  <a href="services.php" class="btn btn-warning">Read More <i class="fa fa-chevron-circle-right"></i></a> </article>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--Service Section End--> 
  <!--Intro Text start-->
  <div class="container">
  <section class="welcome-text">
    <div class="left-half hidden-xs"> </div>
    <div class="right-half">
      <article>
        <h2><span>Welcome To</span> A4Z Printing Press</h2>
        <p>A4z is a private company established in the year 2011.with registration no:..............  A4Z is an effort to meet the fullest demand of customers with professional equipment and remarkable technique.</p>
        <a href="#" class="btn btn-warning">Read More <i class="fa fa-chevron-circle-right"></i></a> </article>
    </div>
  </section>
  </div>
  <!--Intro Text End--> 
  <!--Why Choose Us Start-->
  <section class="why-choose">
    <div class="container">
      <div class="title">
        <h3>Why Choose Us?</h3>
        <p class="lead">Specialty media that expand your capabilities. Workflow solutions
          that streamline your processes.</p>
      </div>
      <div class="row">
        <div class="col-md-6 col-xs-12">
          <div class="choose-list"> <i class="fa fa-hand-o-right"></i>
            <h4>Graphic Designs</h4>
            <p>Eadem fortitudinis ratio reperietur. Urgent tamen et nihil remittunt. pellentesque eu</p>
          </div>
        </div>
        <div class="col-md-6 col-xs-12">
          <div class="choose-list"> <i class="fa fa-hand-o-right"></i>
            <h4>High Quality Flex, Stickers, Photos Print</h4>
            <p>Eadem fortitudinis ratio reperietur. Urgent tamen et nihil remittunt. pellentesque eu</p>
          </div>
        </div>
        <div class="col-md-6 col-xs-12">
          <div class="choose-list"> <i class="fa fa-hand-o-right"></i>
            <h4>High Quality Color Print by Lager Printers</h4>
            <p>Eadem fortitudinis ratio reperietur. Urgent tamen et nihil remittunt. pellentesque eu</p>
          </div>
        </div>
        <div class="col-md-6 col-xs-12">
          <div class="choose-list"> <i class="fa fa-hand-o-right"></i>
            <h4>Cup, Plet, Marbal, T-Shirt Print</h4>
            <p>Eadem fortitudinis ratio reperietur. Urgent tamen et nihil remittunt. pellentesque eu</p>
          </div>
        </div>
        <div class="col-md-6 col-xs-12">
          <div class="choose-list"> <i class="fa fa-hand-o-right"></i>
            <h4>Most Fastest & Quality Print & Photocopy</h4>
            <p>Eadem fortitudinis ratio reperietur. Urgent tamen et nihil remittunt. pellentesque eu</p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--Why Choose Us Start-->
      <div class="container">
  <section class="quotation parallax text-center hidden-xs hidden-sm">

    <div class="row"><div class="col-md-8 col-md-offset-2"><h2>No Electricity, No Problem</h2>
      <p>The only one place Up to A0 size photocopy and printing (B/W) in laser print and colour print up to A3 size with lamination up to A0 size and scanning up to A3 size also available in pokhara.</p></div></div>

  </section>
    </div>
  <!--Our Work Start-->
  <section class="our-work">
    <div class="container">
      <div class="title">
        <h3>Our Work</h3>
        <p class="lead">Specialty media that expand your capabilities. Workflow solutions
          that streamline your processes.</p>
      </div>
      <!-- isotope filters start -->
      <div class="filters">
        <ul class="nav nav-pills">
          <li class="active"><a href="#" data-filter="*">All Designs</a></li>
          <li><a href="#" data-filter=".web-design">Web design</a></li>
          <li><a href="#" data-filter=".app-development">App development</a></li>
        </ul>
      </div>
      <!-- isotope filters end -->
      <div class="isotope-container row grid-space-10">
        <div class="col-sm-6 col-md-4 isotope-item web-design">
          <div class="box-style-1 white-bg">
            <div class="overlay-container"> <img src="img/1.jpg" alt=""> <a href="img/1.jpg" class="overlay small popup-img"> <i class="fa fa-search-plus"></i> </a> </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-4 isotope-item app-development">
          <div class="box-style-1 white-bg">
            <div class="overlay-container"> <img src="img/43365829f399dcc14d1e3de9e3fe2a7f.jpg" alt=""> <a href="img/43365829f399dcc14d1e3de9e3fe2a7f.jpg" class="overlay small popup-img"> <i class="fa fa-search-plus"></i> </a> </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-4 isotope-item web-design">
          <div class="box-style-1 white-bg">
            <div class="overlay-container"> <img src="img/Photocopy-Machine.jpg" alt=""> <a href="img/Photocopy-Machine.jpg" class="overlay small popup-img"> <i class="fa fa-search-plus"></i> </a> </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-4 isotope-item web-design">
          <div class="box-style-1 white-bg">
            <div class="overlay-container"> <img src="img/1.jpg" alt=""> <a href="img/1.jpg" class="overlay small popup-img"> <i class="fa fa-search-plus"></i> </a> </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-4 isotope-item app-development">
          <div class="box-style-1 white-bg">
            <div class="overlay-container"> <img src="img/43365829f399dcc14d1e3de9e3fe2a7f.jpg" alt=""> <a href="img/43365829f399dcc14d1e3de9e3fe2a7f.jpg" class="overlay small popup-img"> <i class="fa fa-search-plus"></i> </a> </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-4 isotope-item web-design">
          <div class="box-style-1 white-bg">
            <div class="overlay-container"> <img src="img/Photocopy-Machine.jpg" alt=""> <a href="img/Photocopy-Machine.jpg" class="overlay small popup-img"> <i class="fa fa-search-plus"></i> </a> </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--Our Work End--> 
  <?php include("footer.php")?>