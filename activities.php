<?php include("header.php")?>
      <div class="caption text-center padding-20">
        <h2>Our Work</h2>
        <ol class="breadcrumb pull-right">
          <li><a href="index.php">Home</a></li>
          <li class="active">Our Work</li>
        </ol>
      </div>
    </div>
  </div>
  <!--Inner page content-->
  <section class="our-work padding-20">
    <div class="container">
      <div class="title">
        <h3>Our Work</h3>
        <p class="lead">Specialty media that expand your capabilities. Workflow solutions
          that streamline your processes.</p>
      </div>
      <!-- isotope filters start -->
      <div class="filters">
        <ul class="nav nav-pills">
          <li class="active"><a href="#" data-filter="*">All Designs</a></li>
          <li><a href="#" data-filter=".web-design">Web design</a></li>
          <li><a href="#" data-filter=".app-development">App development</a></li>
        </ul>
      </div>
      <!-- isotope filters end -->
      <div class="isotope-container row grid-space-10">
        <div class="col-sm-6 col-md-4 isotope-item web-design">
          <div class="box-style-1 white-bg">
            <div class="overlay-container"> <img src="img/Subhakamana-for-kanchan1.jpg" alt=""> <a href="img/Subhakamana-for-kanchan1.jpg" class="overlay small popup-img"> <i class="fa fa-search-plus"></i> </a> </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-4 isotope-item app-development">
          <div class="box-style-1 white-bg">
            <div class="overlay-container"> <img src="img/New-Add-Design1.jpg" alt=""> <a href="img/New-Add-Design1.jpg" class="overlay small popup-img"> <i class="fa fa-search-plus"></i> </a> </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-4 isotope-item web-design">
          <div class="box-style-1 white-bg">
            <div class="overlay-container"> <img src="img/Photocopy-Machine.jpg" alt=""> <a href="img/Photocopy-Machine.jpg" class="overlay small popup-img"> <i class="fa fa-search-plus"></i> </a> </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-4 isotope-item web-design">
          <div class="box-style-1 white-bg">
            <div class="overlay-container"> <img src="img/1.jpg" alt=""> <a href="img/1.jpg" class="overlay small popup-img"> <i class="fa fa-search-plus"></i> </a> </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-4 isotope-item app-development">
          <div class="box-style-1 white-bg">
            <div class="overlay-container"> <img src="img/43365829f399dcc14d1e3de9e3fe2a7f.jpg" alt=""> <a href="img/43365829f399dcc14d1e3de9e3fe2a7f.jpg" class="overlay small popup-img"> <i class="fa fa-search-plus"></i> </a> </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-4 isotope-item web-design">
          <div class="box-style-1 white-bg">
            <div class="overlay-container"> <img src="img/Photocopy-Machine.jpg" alt=""> <a href="img/Photocopy-Machine.jpg" class="overlay small popup-img"> <i class="fa fa-search-plus"></i> </a> </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--Inner page content-->
  <?php include("footer.php")?>
