<?php include("header.php")?>
<div class="container">
  <div class="caption page-top clearfix">
    <div class="pull-left">
      <h2>Company Profile</h2>
    </div>
    <ol class="breadcrumb pull-right">
      <li><a href="index.php">Home</a></li>
      <li class="active">Company Profile</li>
    </ol>
  </div>
</div>

<!--Inner page content-->
<section class="inner-content padding-20">
<div class="container">
  <div class="row">
    <div class="col-md-6">
      <div class="title">
        <h2><span>Introduction of</span> A4Z PRINTING PRESS</h2>
        <p class="lead">A4z is a private company established in the year 2011.with registration no:..............  A4Z is an effort to meet the fullest demand of customers with professional equipment and remarkable technique.</p>
        <div class="border"></div>
      </div>
      <p>A4z Press offers the most comprehensive printing solutions and specializes in printing Books, Magazines, Catalogue, Leaflets/Flyers, Brochures, Calendars, Posters, Mailers, Journals/News Letters, Wedding/Invitation Cards, Post Cards, Labels, Business Cards, Diaries, Digital Banners printing and innumerable other printing jobs. This made possible only due to use of all-digital innovative printing solutions and the latest printing infrastructure available with us.</p>
    </div>
    <div class="col-md-6"> <img src="img/12366337_636061916535607_6435173714761265888_n.jpg" alt=""> </div>
  </div>
  </section>
  <!--Inner page content-->
  <?php include("footer.php")?>
