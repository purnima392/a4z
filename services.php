<?php include("header.php")?>
      <div class="container"><div class="caption page-top clearfix">
        <div class="pull-left"><h2>Our Services</h2></div>
        <ol class="breadcrumb pull-right">
          <li><a href="index.php">Home</a></li>
          <li class="active">Our Services</li>
        </ol>
      </div></div>
    </div>
  </div>
  <!--Inner page content-->
  <section class="our-work padding-20">
    <div class="container">
      <div class="title">
        <h3>Our Services</h3>
        <p class="lead">Specialty media that expand your capabilities. Workflow solutions
          that streamline your processes.</p>
      </div>
      
      <div class="service-list">
      	<div class="row">
        	<div class="col-md-3">
            	<div class="service-box">
                	<div class="image-wrapper"><img src="img/timthumb.png" alt=""></div>
                    <div class="service-detail">
                    	<h3><a href="#">XEROX C75</a></h3>
                        <p>A Versatile Solution for a
Stronger Business</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
            	<div class="service-box">
                	<div class="image-wrapper"><img src="img/timthumb (2).jpg" alt=""></div>
                    <div class="service-detail">
                    	<h3><a href="#">T-Shirt Print</a></h3>
                        <p>A Versatile Solution for a
Stronger Business</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
            	<div class="service-box">
                	<div class="image-wrapper"><img src="img/timthumb (3).jpg" alt=""></div>
                    <div class="service-detail">
                    	<h3><a href="#">Cup Print</a></h3>
                        <p>A Versatile Solution for a
Stronger Business</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
            	<div class="service-box">
                	<div class="image-wrapper"><img src="img/timthumb (4).jpg" alt=""></div>
                    <div class="service-detail">
                    	<h3><a href="#">Photo & Stickers Print</a></h3>
                        <p>A Versatile Solution for a
Stronger Business</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
            	<div class="service-box">
                	<div class="image-wrapper"><img src="img/img1.jpg" alt=""></div>
                    <div class="service-detail">
                    	<h3><a href="#">Flex Print</a></h3>
                        <p>A Versatile Solution for a
Stronger Business</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
            	<div class="service-box">
                	<div class="image-wrapper"><img src="img/PE_film_blowing_machine_3c.jpg" alt=""></div>
                    <div class="service-detail">
                    	<h3><a href="#">Lamination</a></h3>
                        <p>A Versatile Solution for a
Stronger Business</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
            	<div class="service-box">
                	<div class="image-wrapper"><img src="img/Think-business-cards.jpg" alt=""></div>
                    <div class="service-detail">
                    	<h3><a href="#">Visiting Card</a></h3>
                        <p>A Versatile Solution for a
Stronger Business</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
            	<div class="service-box">
                	<div class="image-wrapper"><img src="img/special1.png" alt=""></div>
                    <div class="service-detail">
                    	<h3><a href="#">PVC Card</a></h3>
                        <p>A Versatile Solution for a
Stronger Business</p>
                    </div>
                </div>
            </div>
            
            <div class="col-md-3">
            	<div class="service-box">
                	<div class="image-wrapper"><img src="img/electronic-time-stamp-concept1.jpg" alt=""></div>
                    <div class="service-detail">
                    	<h3><a href="#">Digital Stamp</a></h3>
                        <p>A Versatile Solution for a
Stronger Business</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
            	<div class="service-box">
                	<div class="image-wrapper"><img src="img/final.jpg" alt=""></div>
                    <div class="service-detail">
                    	<h3><a href="#">Ribbon Batch</a></h3>
                        <p>A Versatile Solution for a
Stronger Business</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
            	<div class="service-box">
                	<div class="image-wrapper"><img src="img/excellence-awards-2014_l.jpg" alt=""></div>
                    <div class="service-detail">
                    	<h3><a href="#">Token of Love</a></h3>
                        <p>A Versatile Solution for a
Stronger Business</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
            	<div class="service-box">
                	<div class="image-wrapper"><img src="img/mega_img4.jpg" alt=""></div>
                    <div class="service-detail">
                    	<h3><a href="#">Plate Print</a></h3>
                        <p>A Versatile Solution for a
Stronger Business</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
            	<div class="service-box">
                	<div class="image-wrapper"><img src="img/offset.jpg" alt=""></div>
                    <div class="service-detail">
                    	<h3><a href="#">Offset Print</a></h3>
                        <p>A Versatile Solution for a
Stronger Business</p>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
  </section>
  <!--Inner page content-->
  <?php include("footer.php")?>
