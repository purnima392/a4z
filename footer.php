 <!--footer start-->
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-md-3 col-xs-6">
          <h2>About Company</h2>
          <span class="footer-logo"> <img src="img/footer-logo.png" alt=""> </span>
          <p>Eadem fortitudinis ratio reperietur. Urgent tamen et nihil remittunt. pellentesque eu</p>
          <a href="#">Read More <i class="fa fa-long-arrow-right"></i></a> </div>
        <div class="col-md-3 col-xs-6">
          <h2>Useful links</h2>
          <ul>
            <li><a href="#">Home</a></li>
            <li><a href="#">About Us</a></li>
            <li><a href="#">Services</a></li>
           	<li><a href="#">Gallery</a></li>
            <li><a href="#">Contact Us</a></li>
          </ul>
        </div>
        <div class="col-md-3 col-xs-6">
          <h2>Follow Us</h2>
          <ul class="social-media">
            <li><a href="#"><i class="fa fa-facebook"></i> </a>Find us on facebook</li>
            <li><a href="#"><i class="fa fa-twitter"></i> </a>Follow us on twitter</li>
            <li><a href="#"><i class="fa fa-linkedin"></i></a>Link on linkedin</li>
          </ul>
        </div>
        <div class="col-md-3 col-xs-6">
          <div class="sitemap">
            <h2>Get In Touch</h2>
            <ul class="footer-contact">
              <li><i class="fa fa-map-marker"></i>Newroad, Pokhara, Nepal</li>
              <li><i class="fa fa-phone"></i>+997-61-522751</li>
              <li><i class="fa fa-envelope"></i><a href="#">a4zgdpp@gmail.com</a></li>
              <li><i class="fa fa-globe"></i><a href="#">www.a4zgdpp.com.np</a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="bottom-footer"> <small>&copy; 2016. All Right Reserved. <a href="#">A4Z Printing Press</a></small> <small class="text-right">Designed By <a href="#">Webpage Nepal</a></small> </div>
    </div>
  </footer>
  <!--footer end--> 
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="js/jquery.min.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="bootstrap/js/bootstrap.min.js"></script> 
<script src="js/jquery.appear.js"></script> 
<script src="js/modernizr.js"></script> 
<script src="js/classie.js"></script> 
<script src="js/search.js"></script> 
<!-- Parallax javascript --> 
<script src="js/jquery.parallax-1.1.3.js"></script> 
<!-- Isotope javascript --> 
<script src="plugins/isotope/js/isotope.min.js"></script> 
<script src="plugins/isotope/isotope.pkgd.min.js"></script> 
<!-- Magnific Popup javascript --> 
<script type="text/javascript" src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script> 
<script src="js/script.js"></script>
</body>
</html>