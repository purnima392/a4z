<?php include("header.php")?>
      <div class="container"><div class="caption page-top clearfix">
        <div class="pull-left"><h2>Contact Us</h2></div>
        <ol class="breadcrumb pull-right">
          <li><a href="index.php">Home</a></li>
          <li class="active">Contact Us</li>
        </ol>
      </div></div>
    </div>
  </div>
  <!--Inner page content-->
  <!-- Importing slider content -->
  <section class="slice">
        <div class="wp-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <div class="section-title-wr">
                            <h4 class="section-title left"><span>Send us a message</span></h4>
                        </div>
                        <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                        </p>
                        
                        <form class="form-light mt-20" role="form">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control" placeholder="Your name">
                            </div>
                            <div class="form-group">
                                <label>Mobile No.</label>
                                <input type="text" class="form-control" id="mobile" placeholder="Mobile Number">
                            </div>
                            <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" class="form-control" placeholder="Email address">
                                    </div>
                            
                            <div class="form-group">
                                <label>Note</label>
                                <textarea class="form-control" placeholder="Write you message here..." style="height:100px;"></textarea>
                            </div>
                            <button type="submit" class="btn btn-base">Send message</button>
                        </form>
                    </div>
                    
                    <div class="col-md-5">
                        <div id="mapCanvas" class="map-canvas no-margin"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3515.6816738374378!2d83.98408141466932!3d28.216982482583607!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3995944d76693d11%3A0xf1f006d89be13640!2sNew+Rd%2C+Pokhara+33700!5e0!3m2!1sen!2snp!4v1480401039246" width="600" height="300" frameborder="0" style="border:0" allowfullscreen></iframe></div>
                        
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="subsection">
                                    <div class="section-title-wr">
                                        <h4 class="section-title left">Kanchan Computer Sewa</h4>
                                    </div>
                                    <div class="contact-info">
                                        <div><i class="fa fa-map-marker"></i>
                                        <p>Newroad, Pokhara, Nepal</p></div>
                                        
                                        <div><i class="fa fa-envelope"></i>
                                        <p><a href="mailto:a4zgdpp@gmail.com">a4zgdpp@gmail.com</a></p>
                                        <p><a href="#">info@a4zgdpp.com.np</a></p>
                                        </div>
                                        
                                        <div><i class="fa fa-mobile"></i>
                                        <p>+977-61-522751</p></div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                        </div>

                        
                        
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
  <!--Inner page content-->
  <?php include("footer.php")?>
