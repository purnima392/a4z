<?php include("header.php")?>
  <div class="container">
    <div class="caption page-top clearfix">
      <div class="pull-left">
        <h2>Management Team</h2>
      </div>
      <ol class="breadcrumb pull-right">
        <li><a href="index.php">Home</a></li>
        <li class="active">Management Team</li>
      </ol>
    </div>
  </div>
  
  <!--Inner page content-->
  <section class="inner-content">
    <div class="container">
      <div class="team padding-20">
        <div class="item-hover circle effect7 left_to_right"><a href="#">
          <div class="img"><img src="img/profile-photo_friendly.jpg" alt="img"></div>
          <div class="info">
            <h2>Chairman/ Founder </h2>
            <h3>Padam Shrestha</h3>
            <!--<p>98460-82226</p>
            <p>laxmi@kanchancomputer.com.np</p>--> 
          </div>
          </a></div>
        <div class="item-hover circle effect7 left_to_right"><a href="#">
          <div class="img"><img src="img/profile-photo_friendly.jpg" alt="img"></div>
          <div class="info">
            <h2>Managing Director</h2>
            <h3>Khusshi Shrestha </h3>
            <!--<p>9856020078</p>
            <p>admin@kanchancomputer.com.np</p>--> 
          </div>
          </a></div>
        <div class="item-hover circle effect7 left_to_right"><a href="#">
          <div class="img"><img src="img/profile-photo_friendly.jpg" alt="img"></div>
          <div class="info">
            <h2>Chairman/ Founder </h2>
            <h3>Padam Shrestha</h3>
            <!--<p>98460-82226</p>
            <p>laxmi@kanchancomputer.com.np</p>--> 
          </div>
          </a></div>
        <div class="item-hover circle effect7 left_to_right"><a href="#">
          <div class="img"><img src="img/profile-photo_friendly.jpg" alt="img"></div>
          <div class="info">
            <h2>Managing Director</h2>
            <h3>Khusshi Shrestha </h3>
            <!--<p>9856020078</p>
            <p>admin@kanchancomputer.com.np</p>--> 
          </div>
          </a></div>
        <div class="item-hover circle effect7 left_to_right"><a href="#">
          <div class="img"><img src="img/profile-photo_friendly.jpg" alt="img"></div>
          <div class="info">
            <h2>Marketing Manager</h2>
            <h3>Krishna Kumar Shrestha</h3>
            <!--<p>98460-82226</p>
            <p>laxmi@kanchancomputer.com.np</p>--> 
          </div>
          </a></div>
        <div class="item-hover circle effect7 left_to_right"><a href="#">
          <div class="img"><img src="img/profile-photo_friendly.jpg" alt="img"></div>
          <div class="info">
            <h2>Graphic Designer</h2>
            <h3>Jiwan Shrestha </h3>
            <!--<p>9856020078</p>
            <p>admin@kanchancomputer.com.np</p>--> 
          </div>
          </a></div>
        <div class="item-hover circle effect7 left_to_right"><a href="#">
          <div class="img"><img src="img/profile-photo_friendly.jpg" alt="img"></div>
          <div class="info">
            <h2>Marketing Officer</h2>
            <h3>Ganesh Shrestha</h3>
            <!--<p>98460-82226</p>
            <p>laxmi@kanchancomputer.com.np</p>--> 
          </div>
          </a></div>
        <div class="item-hover circle effect7 left_to_right"><a href="#">
          <div class="img"><img src="img/profile-photo_friendly.jpg" alt="img"></div>
          <div class="info">
            <h2>Offset Machine Operator</h2>
            <h3>Suman Bhujel</h3>
            <!--<p>9856020078</p>
            <p>admin@kanchancomputer.com.np</p>--> 
          </div>
          </a></div>
          <div class="item-hover circle effect7 left_to_right"><a href="#">
          <div class="img"><img src="img/profile-photo_friendly.jpg" alt="img"></div>
          <div class="info">
            <h2>Binding Master</h2>
            <h3>Srijana Gurung</h3>
            <!--<p>9856020078</p>
            <p>admin@kanchancomputer.com.np</p>--> 
          </div>
          </a></div>
          <div class="item-hover circle effect7 left_to_right"><a href="#">
          <div class="img"><img src="img/profile-photo_friendly.jpg" alt="img"></div>
          <div class="info">
            <h2>Marketing</h2>
            <h3>Krishna Thapa</h3>
            <!--<p>9856020078</p>
            <p>admin@kanchancomputer.com.np</p>--> 
          </div>
          </a></div>
          <div class="item-hover circle effect7 left_to_right"><a href="#">
          <div class="img"><img src="img/profile-photo_friendly.jpg" alt="img"></div>
          <div class="info">
            <h2>Office Boy</h2>
            <h3>Nabin Shrestha </h3>
            <!--<p>9856020078</p>
            <p>admin@kanchancomputer.com.np</p>--> 
          </div>
          </a></div>
          <div class="item-hover circle effect7 left_to_right"><a href="#">
          <div class="img"><img src="img/profile-photo_friendly.jpg" alt="img"></div>
          <div class="info">
            <h2>Accountant</h2>
            <h3>Laxmi Shrestha </h3>
            <!--<p>9856020078</p>
            <p>admin@kanchancomputer.com.np</p>--> 
          </div>
          </a></div>
          <div class="item-hover circle effect7 left_to_right"><a href="#">
          <div class="img"><img src="img/profile-photo_friendly.jpg" alt="img"></div>
          <div class="info">
            <h2>Marketing</h2>
            <h3>Manoj Khadka</h3>
            <!--<p>9856020078</p>
            <p>admin@kanchancomputer.com.np</p>--> 
          </div>
          </a></div>
          <div class="item-hover circle effect7 left_to_right"><a href="#">
          <div class="img"><img src="img/profile-photo_friendly.jpg" alt="img"></div>
          <div class="info">
            <h2>Advisor</h2>
            <h3>Yadunath Banjara </h3>
            <!--<p>9856020078</p>
            <p>admin@kanchancomputer.com.np</p>--> 
          </div>
          </a></div>
          <div class="item-hover circle effect7 left_to_right"><a href="#">
          <div class="img"><img src="img/profile-photo_friendly.jpg" alt="img"></div>
          <div class="info">
            <h2>Screen Print Head</h2>
            <h3>Narayan Gautam</h3>
            <!--<p>9856020078</p>
            <p>admin@kanchancomputer.com.np</p>--> 
          </div>
          </a></div>
          
      </div>
    </div>
  </section>
  <!--Inner page content-->
  <?php include("footer.php")?>
